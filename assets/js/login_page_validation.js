 
 // When the browser is ready...
$(function() {  
   // Setup form validation on the #register-form element
   $("#form").validate({
   
       // Specify the validation rules
       rules: {
           username: {
               required: true,
               email:true
           },
password:{
required:true,
minlength:5
}
       },
       
       // Specify the validation error messages
       messages: {
           username: "Please enter a valid email address",
password: {
               required: "Please provide a password",
               minlength: "Your password must be at least 5 characters long"
           },
},
       
       submitHandler: function(form) {
           form.submit();
       }
   });

 });