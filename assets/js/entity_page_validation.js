// When the browser is ready...
 $(function() {  
    // Setup form validation on the #register-form element
    $("#form").validate({
    
        // Specify the validation rules
        rules: {
			entityname: {
                required: true,
                minlength:5
            },
			clientname: {
                required: true
            }
        },
        
        // Specify the validation error messages
        messages: {
			entityname: {
                required: "Please provide a Entity Name",
                minlength: "Your Client Name must be at least 5 characters long"
            },
			clientname: {
                required: "Please Select Client"
            },
			 },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });