 
 // When the browser is ready...
$(function() {  
   // Setup form validation on the #register-form element
   $("#form").validate({
   
       // Specify the validation rules
       rules: {
           pwd: {
               required: true,
			   minlength:5	               
           },
			cpwd:{
			required:true,
			minlength:5,			
			equalTo: "#pwd"
			}
       },
       
       // Specify the validation error messages
       messages: {
			pwd:{
				required:"Please provide a password",
				minlength: "Your password must be at least 5 characters long"
				},
			cpwd:{
				   required: "Please provide a password",
				   minlength: "Your password must be at least 5 characters long"
					  },
},
       
       submitHandler: function(form) {
           form.submit();
       }
   });

 });