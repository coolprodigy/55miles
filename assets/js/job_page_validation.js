var SITE_URL = 'http://55miles.in/';
var userid=new Array();
$(document).ready(function(){
$(".chosen-select").chosen();
//$(".chosen-select").chosen({width: "20%"});
    $("select.periodtype").change(function(){
        var selectoption = $(".periodtype option:selected").val();
		//alert(selectoption);
		var n =new Date().getFullYear();
		$('#period').empty();
		//$('#period').empty();
		$('#year').empty();
		$("#period").append("<option value=''>Select Period</option>");
		if(selectoption=="2"){
			$("#monthper").hide();
			//$("#yearper").show();
			var i=1991;
			selectoption = 'AY';
			n+=1;
			//$("#period").append("<option value=''>Select Period</option>");
			for(var i=i;i<n+1;i++){  
				var newOption = "<option value='"+selectoption+' '+i+'-'+(i+1)+"'>"+selectoption+' '+i+'-'+(i+1)+"</option>"; 
				$("#period").append(newOption);
			}
		}
		else if (selectoption=='1'){
			$("#monthper").hide();
			//$("#yearper").show();
			var i=1990;
			selectoption = 'FY';
			//$("#period").append("<option value=''>Select Period</option>");
				for(var i=i;i<n+1;i++){   
					var newOption = "<option value='"+selectoption+' '+i+'-'+(i+1)+"'>"+selectoption+' '+i+'-'+(i+1)+"</option>"; 
					$("#period").append(newOption);
				}
				}
		else if (selectoption=='3'){
			$("#monthper").show();
			$("#periodper").show();
				$("#period").append($("<option value=''>Select Month</option><option value='April-September'>April-September</option><option value='October-March'>October-March</option>"));
				var i=1990;
				$("#year").append("<option value=''>Select Year</option>");
				for(var i=i;i<=n+1;i++){   
					var newOption = "<option value='"+i+"'>"+i+"</option>"; 
					$("#year").append(newOption);
				} 
				}
		else if (selectoption=='4'){
			$("#monthper").show();
			$("#periodper").show();
				$("#period").append($("<option value=''>Select Month</option><option value='January-March'>January-March</option><option value='April-June'>April-June</option><option value='October-December'>October-December</option>"));
				var i=1990;
				$("#year").append("<option value=''>Select Year</option>");
				for(var i=i;i<=n+1;i++){   
					var newOption = "<option value='"+i+"'>"+i+"</option>"; 
					$("#year").append(newOption);
				}  
		}
		else if (selectoption=='5'){
			$("#monthper").show();
			$("#periodper").show();
				var months = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
				var j=0;
				$("#period").append("<option value=''>Select Month</option>");
				for (var j=0; j<12 ;j++)
				{
					var newOption1 = "<option value='"+months[j]+"'>"+months[j]+"</option>";
					$("#period").append(newOption1);
				}
				var i=1990;
				$("#year").append("<option value=''>Select Year</option>");
				for(var i=i;i<=n+1;i++){   
					var newOption2 = "<option value='"+i+"'>"+i+"</option>"; 
					$("#year").append(newOption2);
				}
				}
		else
		{
			$("#yearper").show();
			$("#monthper").show();
			$("#period").append("<option value=''>Select Month</option>");
			$("#year").append("<option value=''>Select Year</option>");
		//alert("Bye...");
		}
    });

	 $('#jobtype').blur(function(){
		if ($.trim(this.value) != "")
		{
			$("#jobname").attr("disabled", true);
        }
    	else
	    {
		    $("#jobname").removeAttr("disabled");
	    }
	});

	$('#jobname').blur(function(){
		if ($.trim(this.value) != "")
		{
			 $("#jobtype").attr("disabled", true);
	    }
    	else
	    {
		     $("#jobtype").removeAttr("disabled");
	    }
    });


// just for the demos, avoids form submit
jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
$(function() {  
$( "#form" ).validate({
	//ignore: '*:not([name])',
	//For array validation//ignore: [],
  rules: {
	clientgrp: {
      required: true
    },
	/*clientent: {
      required: true
    },*/
    jobtype: {
      require_from_group: [1,".jobtypename"]
    },
    jobname: {
      require_from_group: [1,".jobtypename"]
    },
	periodtype: {
      required: true
    },
	period: {
      required: true
    },
	year: {
      required: true
    },
	jobmanager: {
      required: true
    },
	useronjob: {
      required: true,
	  minlength:1
    },
	duedate: {
      required: true,
	  date : true
    }
  },
	groups: {
            jobtypename: 'jobtype jobname'
        },
	// Specify the validation error messages
        messages: {
			clientgrp: {
                required: "Please Select Client"
            },
			/*clientent: {
                required: "Please Select Entity"
            },*/
			periodtype: {
                required: "Please Select Period Type"
            },
				period: {
                required: "Please Select Period"
            },
				year: {
                required: "Please Select Period Year"
            },
			jobmanager: {
                required: "Please Select Job Manager"
            },
			useronjob: {
				required: "Please Select Atleast One User",
                minlength: "Please Select Atleast One User"
            },
			duedate: {
                required: "Due Date is Required",
				date : "Enter valid Date"
            },
			 },
		submitHandler: function(form) {
            form.submit();
        }
		})
	  });
	$('#clientgrp').on('change',function(){
		var id=$('#clientgrp').val()?$('#clientgrp').val():0;
		 var web_view=true;
		//alert(id);
		$.getJSON(SITE_URL+'Job/getentity/'+id+'/'+web_view,function(response){
			//alert(response);
			if(response==false){
			
			$('#clientent').empty();
			$('#clientent').append("<option value=''>Select Entity</option>");
			$('#clientent').append("<option value=''>No Entities</option>");
			$('#clientent').trigger("chosen:updated");
			}else{
			var cltent=response;//JSON.parse can be used too.
			//alert(cltent);
			//console.log(cltent);
				$('#clientent').empty();
				$('#clientent').append("<option value=''>Select Entity</option>");
				$.each(cltent,function(index,item){
				$('#clientent').append($("<option></option>").attr("value", item['clt_ent_id']).text(item['clt_ent_name']));
							// OR
				//$('#clientent').append($("<option></option>").attr("value", item.clt_ent_id).text(item.clt_ent_name));
				});

			}
		})
	});
	$('#clientgrp').on('change',function(){
	  var id=$('#clientgrp').val()?$('#clientgrp').val():0;
	   var web_view=true;
	  $.getJSON(SITE_URL+'Job/getuserdefault/'+id+'/'+web_view,function(response){
			//alert(response);
			if(response==false){
			$('#accdefault').empty();
			//$('#accdefault option').attr("selected",false);
			$('#accdefault').append($("<option></option>").attr("value",''));
			$('#accdefault').val('').trigger('chosen:updated');
			}else{
			var userdef=response;//JSON.parse can be used too.
			//alert(cltent);
			//console.log(cltent);
			$('#accdefault').empty();
				$.each(userdef,function(index,item){
				$('#accdefault').append($("<option></option>").attr("value", item['log_id']).text(item['user_fullname']));
				$('#accdefault option').attr("selected",true);
				$('#accdefault').prop('disabled', true).trigger("chosen:updated");
				$('#accdefault').trigger("chosen:updated");
				//console.log('hi');
							// OR
				//$('#clientent').append($("<option></option>").attr("value", item.clt_ent_id).text(item.clt_ent_name));
				});

			}
		})
	});
		$('#clientgrp').on('change',function(){
			 var id=$('#clientgrp').val()?$('#clientgrp').val():0;
			getAccbychoice(id);
		/*$.getJSON(SITE_URL+'Job/getuserchoice/'+id+'/'+web_view,function(response){
			//alert(response);
			if(response==false){
			$('#useronjob').empty();
			$('#useronjob').append("<option value=''>No User</option>");
			$('#useronjob').trigger("chosen:updated");
			}else{
			var userchoice=response;//JSON.parse can be used too.
			//alert(cltent);
			//console.log(cltent);
			$('#useronjob').empty();
				$.each(userchoice,function(index,item){
				$('#useronjob').append($("<option></option>").attr("value", item['log_id']).text(item['user_fullname']));
				// Update Drop-Down ID

				//$('#useronjob').prop('disabled', true).trigger("chosen:updated");
				
				//userid.push(item['log_id']);
				$('#useronjob').trigger("chosen:updated");
				
							// OR
				//$('#clientent').append($("<option></option>").attr("value", item.clt_ent_id).text(item.clt_ent_name));
				});
				//console.log(userid);
				//console.log($('#useronjob').val());
			}
		})*/
	});

		function getAccbychoice(id,flag,chosenvals)
		{		
			var web_view=true;
			var jbmngrid=$('#jobmanager').val();
	$.getJSON(SITE_URL+'Job/getuserchoice/'+id+'/'+web_view,function(response){
			if(response==false){
			$('#useronjob').empty();
			$('#useronjob').append("<option value=''>No User</option>");
			}else{
			var userchoice=response;//JSON.parse can be used too.
			$('#useronjob').empty();
				$.each(userchoice,function(index,item){
				$('#useronjob').append($("<option></option>").attr("value", item['log_id']).text(item['user_fullname']));
				
				$('#useronjob').trigger("chosen:updated");

				$("#useronjob option[value='"+jbmngrid+"']").remove();
				
				});

					if(flag)
						{
							if(chosenvals!=null){
								var index = chosenvals.indexOf(jbmngrid);

									if(index!=-1)
									{
										chosenvals.splice(index, 1);
									}
								//console.log(chosenvals);
								
								chosenvals.forEach(function(selid) {
								
									$('#useronjob option[value='+selid+']').prop('selected', true);
									
								});
							}
							$('#useronjob').trigger("chosen:updated");
						}
			}
		})
	
}

		$('#useronjob_chosen ul.chosen-choices').bind('click', function() {
			$.each(userid, function(index, item) {
				//$('#useronjob_chosen ul.chosen-results li').attr('id', 'optionid_'+item['log_id']);
				//$('#useronjob_chosen ul.chosen-results li').append('<input type="text" id="'+item.log_id+'" value="" />');
				
			});	
		});

		$('#jobmanager').change(function(){
			var id=$('#clientgrp').val()?$('#clientgrp').val():0;
			getAccbychoice(id,1,$("#useronjob").chosen().val());


		})
			
			$('#duedate').datepicker({
				dateFormat:'yy-mm-dd'
			});
});