// When the browser is ready...
 $(function() {  
    // Setup form validation on the #register-form element
    $("#form").validate({
    
        // Specify the validation rules
        rules: {
            clientname: {
                required: true,
                minlength:5
            },
		    clientemail: {
                required: true,
                email:true
            },
			clientnumber: {
                required: true,
				number: true,
                minlength:10,
				maxlength:10
            },
			clientmanager: {
                required: true
            }
        },
        
        // Specify the validation error messages
        messages: {
			clientname: {
                required: "Please provide Client Name",
                minlength: "Your Client Name must be at least 5 characters long"
            },
			clientemail: {
                required: "Please provide Email ID",
                email: "Please enter a valid email address"
            },
			clientnumber: {
                required: "Please provide a Mobile Number",
				number: "Enter Only Digits",
                minlength: "Your password must be 10 Digits",
				maxlength: "Your password must be 10 Digits"
            },
			clientmanager: {
                required: "Please Select Client Manager"
            },
			 },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });