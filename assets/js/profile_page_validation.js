  // When the browser is ready...
 $(function() {  
    // Setup form validation on the #register-form element
    $("#form").validate({
    
        // Specify the validation rules
        rules: {
            fullname: {
                required: true,
                minlength: 5
            },
            email: {
                required: true,
				email:true
            }
        },
        
        // Specify the validation error messages
        messages: {
			fullname: {
                required: "Please provide a fullname",
                minlength: "Your fullname must be at least 5 characters long"
            },
            email: "Please enter a valid email address"
			 },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  