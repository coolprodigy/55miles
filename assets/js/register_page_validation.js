  // When the browser is ready...
 $(function() {  
    // Setup form validation on the #register-form element
    $("#form").validate({
    
        // Specify the validation rules
        rules: {
            fullname: {
                required: true,
                minlength: 5
            },
            email: {
                required: true,
				email:true
            },
			pass:{
				required:true,
				minlength:5
				},
			companyname:{
				required:true,
				minlength:5		
				}
        },
        
        // Specify the validation error messages
        messages: {
			fullname: {
                required: "Please provide a fullname",
                minlength: "Your fullname must be at least 5 characters long"
            },
            email: "Please enter a valid email address",
			pass: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
			companyname: {
                required: "Please provide a companyname",
                minlength: "Your companyname must be at least 5 characters long"
            }
			 },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });