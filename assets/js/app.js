var app=angular.module('myApp',['ngRoute', 'ngAnimate']);
app.config(function($routeProvider){
     
	  $routeProvider
          .when('/c',{
                templateUrl: 'client'
          }).
          when('/e',{
                templateUrl: 'entity'
			 }).
				 when('/u',{
                templateUrl: 'user'
			 }).
				   when('/j',{
                templateUrl: 'job'
			 }).
				  when('/',{
                templateUrl: 'login/lview'
			 }).
				  when('/r',{
                templateUrl: 'register'
			 }).

				otherwise({
				redirectTo: '/'
          });
});
app.controller('validateCtrl',function($scope){
	/*      
    Here you can handle controller for specific route as well.
    */
});
app.controller('eCtrl',function($scope,$http,$httpParamSerializer){
$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';     
	 $scope.sendForm = function() {
    //alert('form sent');
	 //console.log(this.formData);
		$http.post('entity',$httpParamSerializer($scope.formData)).success(function(resp){
		angular.element('#form').html(resp);
		});
  }
	/*      
    Here you can handle controller for specific route as well.
    */
});

app.controller('cCtrl',function($scope,$http,$httpParamSerializer){
$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';     
	 $scope.sendForm = function() {
    //alert('form sent');
	 //console.log(this.formData);
		$http.post('client',$httpParamSerializer($scope.formData)).success(function(resp){
		angular.element('#form').html(resp);
		});
  }
	/*      
    Here you can handle controller for specific route as well.
    */
});

app.controller('uCtrl',function($scope,$http,$httpParamSerializer){
$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';     
	 $scope.sendForm = function() {
    //alert('form sent');
	 //console.log(this.formData);
		$http.post('user',$httpParamSerializer($scope.formData)).success(function(resp){
		angular.element('#form').html(resp);
		});
  }
	/*      
    Here you can handle controller for specific route as well.
    */
});

/*app.controller('rCtrl',function($scope,$http,$httpParamSerializer){
$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';     
	 $scope.sendForm = function() {
    //alert('form sent');
	 //console.log(this.formData);
		$http.post('register',$httpParamSerializer($scope.formData)).success(function(resp){
		angular.element('#form').html(resp);
		});
  }

});
*/


app.controller('jctrl', function($scope, $http,$httpParamSerializer) {
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $scope.years=[];
	$scope.months=[];
	$scope.qyears=[];
    var n =new Date().getFullYear();
    $scope.changedValue=function(item){
	var test=$scope.formData.pt;
    $scope.years.splice(0,n);
        if(test=='AY'){
                i=1991;
                n+=1;
				for(var i=i;i<n+1;i++){   
				$scope.years.push(test+' '+i+'-'+(i+1));
				} 
        }
        else if (test=='FY'){
				var i=1990;
				for(var i=i;i<n+1;i++){   
				$scope.years.push(test+' '+i+'-'+(i+1));
				}           
		}
		else if (test=='HY'){
				$scope.months = ["April-September", "October-March"];
				var i=1990;
				for(var i=i;i<=n;i++){   
				$scope.years.push(i);
				}           
		}
		else if (test=='Q'){
				$scope.months = ["January-March", "April-June","July-September", "October-December"];
				var i=1990;
				for(var i=i;i<=n;i++){   
				$scope.years.push(i);
				}           
		}
		else if (test=='M'){
				$scope.months = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
				var i=1990;
				for(var i=i;i<=n;i++){   
				$scope.years.push(i);
				}           
		}
	}
$scope.getentity = function(name){
//alert(name)
$scope.entities = [];
//$http.defaults.headers.post["Content-Type"] = 'application/x-www-form-urlencoded;charset=utf-8';
//$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
$http.post("job/getentity", $.param({'data' : name})).success(function($data){$scope.entities=$data; });
}
	$scope.sendForm = function() {
    //alert('form sent');
	 //console.log(this.formData);
		$http.post('job',$httpParamSerializer($scope.formData)).success(function(resp){
			alert(resp);
		angular.element('#form').html(resp);
		});
  }
});

