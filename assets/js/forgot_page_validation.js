 
 // When the browser is ready...
$(function() {  
   // Setup form validation on the #register-form element
   $("#form").validate({
   
       // Specify the validation rules
       rules: {
           username: {
               required: true,
               email:true
           }
       },
       
       // Specify the validation error messages
       messages: {
           username: "Please enter a valid email address"
},
       
       submitHandler: function(form) {
           form.submit();
       }
   });

 });