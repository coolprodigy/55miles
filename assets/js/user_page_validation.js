// When the browser is ready...
 $(function() {  
    // Setup form validation on the #register-form element
    $("#form").validate({
    
        // Specify the validation rules
        rules: {
            username: {
                required: true,
                minlength:5
            },
		    useremail: {
                required: true,
                email:true
            },
			usernumber: {
                required: true,
				number: true,
                minlength:10,
				maxlength:10
            },
			userrole: {
                required: true
            }
        },
        
        // Specify the validation error messages
        messages: {
			username: {
                required: "Please provide Full Name",
                minlength: "Your Full Name must be at least 5 characters long"
            },
			useremail: {
                required: "Please provide Email ID",
                email: "Please enter a valid email address"
            },
			usernumber: {
                required: "Please provide a Mobile Number",
				number: "Enter Only Digits",
                minlength: "Your password must be 10 Digits",
				maxlength: "Your password must be 10 Digits"
            },
			userrole: {
                required: "Please Select User Type"
            },
			 },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });