-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2015 at 11:52 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `55milesrest`
--

-- --------------------------------------------------------

--
-- Table structure for table `ca_firm`
--

CREATE TABLE IF NOT EXISTS `ca_firm` (
  `ca_id` int(10) NOT NULL AUTO_INCREMENT,
  `ca_name` varchar(100) NOT NULL,
  `ca_add_dt` datetime NOT NULL,
  `ca_modf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ca_id`)
) TYPE=InnoDB ;

--
-- Dumping data for table `ca_firm`
--

INSERT INTO `ca_firm` (`ca_id`, `ca_name`, `ca_add_dt`, `ca_modf_dt`) VALUES
(1, 'Softpillar tech', '2015-08-27 16:03:53', '0000-00-00 00:00:00'),
(2, 'Softpillar', '2015-09-02 16:05:17', '0000-00-00 00:00:00'),
(3, 'Softpillar', '2015-09-02 16:07:05', '0000-00-00 00:00:00'),
(4, 'Softpillar', '2015-09-02 16:07:32', '0000-00-00 00:00:00'),
(5, 'Softpillar tech', '2015-09-02 16:23:20', '0000-00-00 00:00:00'),
(6, 'Softpillar tech', '2015-09-02 16:24:10', '0000-00-00 00:00:00'),
(7, 'Softpillar tech', '2015-09-02 16:25:45', '0000-00-00 00:00:00'),
(8, 'Softpillar tech', '2015-09-02 16:25:53', '0000-00-00 00:00:00'),
(9, 'Softpillar tech', '2015-09-02 16:29:54', '0000-00-00 00:00:00'),
(10, 'Softpillar 123', '2015-09-02 17:49:50', '2015-09-03 17:08:22'),
(11, '55miles', '2015-09-03 15:42:44', '0000-00-00 00:00:00'),
(12, 'Futurz', '2015-09-08 12:14:46', '0000-00-00 00:00:00'),
(13, 'Pratish Solutions', '2015-09-08 12:16:29', '0000-00-00 00:00:00'),
(15, 'VFX Animations', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Freelancing', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Nilu Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `client_entity`
--

CREATE TABLE IF NOT EXISTS `client_entity` (
  `clt_ent_id` int(10) NOT NULL AUTO_INCREMENT,
  `clt_grp_id` int(10) NOT NULL,
  `clt_ent_name` varchar(100) NOT NULL,
  `ent_add_dt` datetime NOT NULL,
  `ent_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`clt_ent_id`)
) TYPE=InnoDB ;

--
-- Dumping data for table `client_entity`
--

INSERT INTO `client_entity` (`clt_ent_id`, `clt_grp_id`, `clt_ent_name`, `ent_add_dt`, `ent_mdf_dt`) VALUES
(1, 5, 'Satish Power PVT LTD', '2015-09-09 13:22:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `client_group`
--

CREATE TABLE IF NOT EXISTS `client_group` (
  `clt_grp_id` int(10) NOT NULL AUTO_INCREMENT,
  `clt_grp_name` varchar(100) NOT NULL,
  `clt_number` varchar(50) NOT NULL,
  `clt_email` varchar(100) NOT NULL,
  `clt_added_by` int(10) NOT NULL,
  `clt_mngr_id` int(10) NOT NULL,
  `clt_cont_person` varchar(100) NOT NULL,
  `ca_id` int(10) NOT NULL,
  `clt_add_dt` datetime NOT NULL,
  `clt_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`clt_grp_id`)
) TYPE=InnoDB ;

--
-- Dumping data for table `client_group`
--

INSERT INTO `client_group` (`clt_grp_id`, `clt_grp_name`, `clt_number`, `clt_email`, `clt_added_by`, `clt_mngr_id`, `clt_cont_person`, `ca_id`, `clt_add_dt`, `clt_mdf_dt`) VALUES
(1, '55miles technologies Private Limited', '2147483647', '55milles@gmail.com', 2, 2, '', 1, '2015-09-07 15:45:32', '0000-00-00 00:00:00'),
(2, 'Softpillar', '9856231478', 'abc@siftpillar.com', 2, 11, 'Amit', 1, '2015-09-07 18:44:36', '0000-00-00 00:00:00'),
(4, 'Microsoft', '9856231475', 'sumit@microsoft.com', 2, 12, 'Kiran', 1, '2015-09-08 11:21:16', '0000-00-00 00:00:00'),
(5, 'Amit Solution PVT LTD', '9999999999', 'amit@abc.com', 12, 2, 'abc', 1, '2015-09-09 12:50:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `job_id` int(10) NOT NULL AUTO_INCREMENT,
  `prd_id` int(10) NOT NULL,
  `job_type_id` int(10) NOT NULL,
  `job_mngr_id` int(10) NOT NULL,
  `job_asgnd_by` int(10) NOT NULL,
  `job_add_dt` datetime NOT NULL,
  `job_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`job_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `job_type`
--

CREATE TABLE IF NOT EXISTS `job_type` (
  `job_type_id` int(10) NOT NULL AUTO_INCREMENT,
  `job_type_name` varchar(100) NOT NULL,
  `prd_id` int(10) NOT NULL,
  `job_added_by` int(10) NOT NULL,
  `job_add_dt` datetime NOT NULL,
  `job_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`job_type_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `period_type`
--

CREATE TABLE IF NOT EXISTS `period_type` (
  `prd_id` int(10) NOT NULL AUTO_INCREMENT,
  `prd_name` varchar(100) NOT NULL,
  `prd_add_dt` datetime NOT NULL,
  `prd_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`prd_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `rights_id` int(10) NOT NULL AUTO_INCREMENT,
  `rights_label` varchar(100) NOT NULL,
  `rights_add_dt` datetime NOT NULL,
  `rights_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rights_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(100) NOT NULL,
  `rights_id` int(10) NOT NULL,
  `role_add_dt` datetime NOT NULL,
  `role_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`role_id`)
) TYPE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `ca_id` int(10) NOT NULL,
  `log_email` varchar(100) NOT NULL,
  `pwd` tinytext NOT NULL,
  `log_status` enum('0','1') NOT NULL DEFAULT '1',
  `role_id` enum('1','2','3','4','5','6') NOT NULL,
  `log_add_dt` datetime NOT NULL,
  `log_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_email` (`log_email`)
) TYPE=InnoDB ;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`log_id`, `ca_id`, `log_email`, `pwd`, `log_status`, `role_id`, `log_add_dt`, `log_mdf_dt`) VALUES
(1, 0, '0f61ca92efd71ce6ea397244c1e4e18a', 'bdf6ad69a622e26f5934aa6d3ca32a9f', '1', '', '2015-08-26 17:10:14', '2015-09-01 18:46:01'),
(2, 1, 'sumitnalwala3@gmail.com', '8756124e47e3798dc41c8a5f5f485500', '1', '1', '2015-08-27 16:03:53', '2015-09-08 12:12:27'),
(3, 9, '8817cdf1923712c664e5555a2a10cde7', 'e4e9c51dd8a82cc429244046db282d76', '1', '', '2015-09-02 16:29:54', '2015-09-02 16:34:00'),
(4, 1, 'nileshpatil491@gmail.com', '8756124e47e3798dc41c8a5f5f485500', '1', '2', '2015-09-02 17:49:50', '2015-09-09 13:53:28'),
(5, 1, 'sunil.goilkar41@gmail.com', 'bc76ab38116878f44051f01cff9254be', '1', '2', '2015-09-03 15:42:44', '2015-09-09 13:53:45'),
(13, 12, 'ajinkya@gmail.com', 'bf65d95b5e56a4a21cd042e1377a4a07', '1', '1', '2015-09-08 12:14:46', '0000-00-00 00:00:00'),
(14, 13, 'pratish@sofpillar.com', 'bf65d95b5e56a4a21cd042e1377a4a07', '1', '1', '2015-09-08 12:16:29', '0000-00-00 00:00:00'),
(16, 15, 'abc@abc.com', '8756124e47e3798dc41c8a5f5f485500', '1', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, 'satish@gmail.com', '', '1', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 16, 'atish@gmail.com', '8756124e47e3798dc41c8a5f5f485500', '1', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 1, 'ajinkya21@gmail.com', '', '1', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 17, 'nilu@phule.com', '8756124e47e3798dc41c8a5f5f485500', '1', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 1, 'nilya@nilya.com', '', '1', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_metadata`
--

CREATE TABLE IF NOT EXISTS `user_metadata` (
  `info_id` int(10) NOT NULL AUTO_INCREMENT,
  `log_id` int(10) NOT NULL,
  `user_fullname` varchar(100) NOT NULL,
  `user_fname` varchar(100) NOT NULL,
  `user_lname` varchar(100) NOT NULL,
  `user_desg` varchar(100) NOT NULL,
  `user_number` varchar(50) NOT NULL,
  `user_hour_cost` varchar(25) NOT NULL,
  `newsletter` enum('0','1') NOT NULL DEFAULT '1',
  `user_add_dt` datetime NOT NULL,
  `user_mdf_dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`info_id`)
) TYPE=InnoDB ;

--
-- Dumping data for table `user_metadata`
--

INSERT INTO `user_metadata` (`info_id`, `log_id`, `user_fullname`, `user_fname`, `user_lname`, `user_desg`, `user_number`, `user_hour_cost`, `newsletter`, `user_add_dt`, `user_mdf_dt`) VALUES
(1, 2, 'Sumit Nalwala', '', '', '', '0', '', '0', '2015-08-27 16:03:53', '2015-09-08 15:10:12'),
(2, 3, 'Sunil Goilkar', '', '', '', '0', '', '0', '2015-09-02 16:29:54', '2015-09-02 16:34:07'),
(8, 4, 'NIlesh Patil', '', '', '', '0', '', '0', '2015-09-02 17:49:50', '2015-09-09 13:55:08'),
(9, 5, 'Sunil Goilkar', '', '', '', '0', '', '0', '2015-09-03 15:42:44', '2015-09-09 15:59:53'),
(10, 13, 'Ajinkya Pawar', '', '', '', '0', '', '0', '2015-09-08 12:14:46', '0000-00-00 00:00:00'),
(11, 14, 'Pratish Rupawate', '', '', '', '0', '', '0', '2015-09-08 12:16:29', '0000-00-00 00:00:00'),
(13, 16, 'Nishikant Kamat', '', '', '', '', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 17, 'Satish Powar', '', '', 'Designer', '9856235689', '2000', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 18, 'Atish Rupawate', '', '', '', '', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 21, 'Ajinkay Chaturvedi', '', '', 'Developer', '8956235689', '1500', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 22, 'Nilu Phule', '', '', '', '', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 23, 'Nilya Phule', '', '', 'Senior php developer', '8956234578', '1', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `role_id` int(2) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) NOT NULL,
  `role_add_dt` timestamp NOT NULL,
  `role_mdf_dt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`role_id`)
) TYPE=InnoDB ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`role_id`, `role_name`, `role_add_dt`, `role_mdf_dt`) VALUES
(1, 'Superadmin', '2015-09-10 06:03:16', '2015-09-10 06:05:49'),
(2, 'Partner', '2015-09-10 06:03:16', '2015-09-10 06:05:35'),
(3, 'Supervisor', '2015-09-10 06:03:16', '2015-09-10 06:05:35'),
(4, 'Manager', '2015-09-10 06:03:16', '2015-09-10 06:05:35'),
(5, 'Assistant', '2015-09-10 06:03:16', '2015-09-10 06:05:35'),
(6, 'Account Assistant', '2015-09-10 06:03:16', '2015-09-10 06:05:35');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
