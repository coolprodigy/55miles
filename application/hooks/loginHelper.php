<?php
class SessionData 
{
    var $CI;

    function __construct(){
         $this->CI =& get_instance();
		
	}

    function initializeData() {
			$class=$this->CI->router->fetch_class();
			if(!$this->CI->session->userdata('logged_in') && ($class!='Login') && ($class!='Register') && ($class!='Forgot'))
			{	
				redirect('Login','refresh');
			}
	}

}
?>