<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Do_register extends CI_Model {
	public function __construct() 
	{
	parent::__construct();
	}
	
	#This function will insert the record of the customer/CA into the database.
	public function insert_customer($personal,$credentials,$firm,$user=null){
	#echo 'hi';die;
		if(!$user){
			$sql=$this->db->insert('ca_firm', $firm);
			$credentials['ca_id']=$this->db->insert_id();
		}
		if(isset($sql) || isset($user)){			
			$user_cred=$this->db->insert('user_login',$credentials);
			$personal['log_id']=$this->db->insert_id();
			$user_pers=$this->db->insert('user_metadata',$personal);
			return true;
		}
	
	}

	#This function will check for the email existence.
	public function email_check($email){
	$sql=$this->db->get_where('user_login',array('log_email'=>$email));
	return $sql->result();
	}
}