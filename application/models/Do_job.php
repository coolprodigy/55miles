<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Do_job extends CI_Model {
	public function __construct() 
	{
	parent::__construct();
	}
	
	#This function will get the entity respective to the client group.
	public function getentity($id){
	return $sql=$this->db->query("SELECT clt_ent_name,clt_ent_id from client_entity where clt_grp_id=".$id)->result_array();
	}
	#This function will get the job type.
	public function getjobtype(){
	return $sql=$this->db->query('SELECT job_type_id,job_type_name from job_type')->result_array();
	}
	
	#This function will get job manager.
	public function getjobmanager(){
	$ca_id=$this->session->userdata('logged_in')['ca_id'];
	return $sql=$this->db->query("SELECT a.log_id,a.log_email,b.user_fullname FROM user_login a left join user_metadata b on a.log_id=b.log_id where ca_id='".$ca_id."' and (a.role_id=1 or a.role_id=2 or a.role_id=3)")->result_array();
	}
	
	#This function is used to get article and manager so that they can be attached on job.
	public function getuserchoice($id){
		$ca_id=$this->session->userdata('logged_in')['ca_id'];
		return $this->db->query("SELECT a.log_id,a.log_email,b.user_fullname FROM user_login a left join user_metadata b on a.log_id=b.log_id where ca_id='".$ca_id."' and (role_id=3 or role_id=4) and a.log_id!=(select clt_mngr_id from client_group where clt_grp_id='".$id."') ")->result_array();
		
	}
	
	#This function will get default user.
	public function getuserdefault($id){
		$ca_id=$this->session->userdata('logged_in')['ca_id'];
		return $sql=$this->db->query("SELECT a.log_id,a.log_email,b.user_fullname FROM user_login a left join user_metadata b on a.log_id=b.log_id where ca_id='".$ca_id."' and (role_id=1 or role_id=2) or a.log_id=(select clt_mngr_id from client_group where clt_grp_id='".$id."' )")->result_array();
	}
	
	#This function is used for adding the job.
	public function addjob($job,$job_meta){
	$job_asgnd_by=$this->session->userdata('logged_in')['log_id'];
	$add_job=$this->db->insert('job',$job);
	$job_id=$this->db->insert_id();
	$job_meta['job_id']=$job_id;
	$add_job_meta=$this->db->insert('job_metadata',$job_meta);
	}

	#This function is used for getting period type.
	public function getprdtype(){
	return $sql=$this->db->query('SELECT prd_id,prd_val,prd_name from period_type')->result_array();
	}
}