<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Do_profile extends CI_Model {
	public function __construct() 
	{
	parent::__construct();
	}
	
	#This function will fetch the user details.
	public function userdetails(){
	$userid=$this->session->userdata('logged_in')['log_id'];
	return $sql=$this->db->query("Select a.log_email,b.user_fullname from user_login a left join user_metadata b on a.log_id=b.log_id where a.log_id=".$userid)->row_array();
	}
	
	#This function will edit the user details.
	public function edituserdetails($fullname,$email,$userid){
	$this->db->where('log_id', $userid);
	$per_fname=$this->db->update('user_metadata',$fullname);
	$this->db->where('log_id', $userid);
	$per_email=$this->db->update('user_login',$email);
	return true;
	}
}