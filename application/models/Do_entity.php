<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Do_entity extends CI_Model {
	public function __construct() 
	{
	parent::__construct();
	}
	
	#this function is used to retrieve all the clients related to a particular CA/Customer.
	public function getclientgrp(){
	$ca_id=$this->session->userdata('logged_in')['ca_id'];		
	return $sql=$this->db->query("SELECT clt_grp_id,clt_grp_name from client_group where ca_id='".$ca_id."'")->result_array();
	}
	
	#This function inserts the entity details into the database.
	public function addentity($entity_det){
	return $sql=$this->db->insert('client_entity',$entity_det);
	}
}