<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	require APPPATH.'/libraries/REST_Controller.php';
	class Logout extends REST_Controller {
		function __Construct(){
		parent:: __construct();
	}

	#This function is used for log out.
	public function expire_get(){
		unset($_COOKIE['login']);
		unset($_COOKIE['loginpwd']);
		setcookie('login', null, -1, '/');
		setcookie('loginpwd', null, -1, '/');
		#setcookie('login','', time() + (86400 * 7), "/");
		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		redirect('Login', 'refresh');
	}
	
	
}