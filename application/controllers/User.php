<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class User extends REST_Controller {
		function __Construct(){
		parent::__construct();
		$this->load->model('Do_user');
		$this->load->model('Do_register');
		//$this->session->set_userdata('is_in_login_page',false);
		}
	
		#This function will load the view using get request.
		public function index_get(){
		$this->load->view('user_page');
		}
		
#This function validates the registration field and allows to insert into the database		
		public function adduser_post()
        {		
				$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">','</div>');
				$this->form_validation->set_rules('username','User Name','required|min_length[5]');
				$this->form_validation->set_rules('useremail','User Email','required|valid_email');
				$this->form_validation->set_rules('usernumber','User Number','required|numeric|min_length[10]|max_length[10]');
				$this->form_validation->set_rules('userrole','User type','required');
				if($this->form_validation->run()==false){
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
					$controller['object']=$this;
					$this->load->view('user_page');
					}else{
					$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
					$this->response($response,OK);
					}//Done testing
					
				}
			else{	
				$personal=array(
				'user_fullname'=>$this->input->post('username'),			
				'user_number'=>$this->input->post('usernumber'),
				'user_desg'=>$this->input->post('userdesg'),
				'user_hour_cost'=>$this->input->post('usermanhrcost')
				);
				$credentials=array(
				'log_email'=>$this->input->post('useremail'),
				'role_id'=>$this->input->post('userrole'),
				'ca_id'=>$this->session->userdata('logged_in')['ca_id']			
				);
				$firm=null;
				$user=true;
				#print_r($credentials);
				#print_R($_POST);die;
				$result=$this->Do_register->insert_customer($personal,$credentials,$firm,$user);
				if($result){
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
						echo 'User has been added successfully';
					}else{
					$response=array('success'=>true,'response'=>array('message'=>'User has been added successfully'));
					$this->response($response,OK);
					}
				}
			}	
        }
		/*public function getusertype_get(){
		return $result=$this->Do_user->getusertype();
		}*/
		public function getusertype_get($web_view=null){
			if($web_view){
			return $this->Do_user->getusertype();
			}
			else{
			$this->response($this->Do_user->getusertype(),OK);	
			}
		}

		#This function will load the view when the user hits the link of browser using get request.
		public function adduser_get(){
		$this->load->view('user_page');
		}
}