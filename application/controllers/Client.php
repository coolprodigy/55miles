<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Client extends REST_Controller {
		function __Construct(){
		parent::__construct();
		$this->load->model('Do_client');
		//$this->session->set_userdata('is_in_login_page',false);
		}
	
		#This function will load the view using get request.
		public function index_get(){
		$this->load->view('client_page');
		}
		
#This function validates the registration field and allows to insert into the database		
		public function addclient_post()
        {		
				$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">','</div>');
				$this->form_validation->set_rules('clientname','Client Name','required|min_length[5]');
				$this->form_validation->set_rules('clientemail','Client Email ID','required|valid_email');
				$this->form_validation->set_rules('clientnumber','Client Number','required|numeric|min_length[10]|max_length[10]');
				$this->form_validation->set_rules('clientmanager','Client Manager','required');
				if($this->form_validation->run()==false){
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
					$this->load->view('client_page');
					}else{
					$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
					$this->response($response,OK);
					}//Done testing
					
				}
			else{	
				$clientdetails=array(
				'clt_grp_name'=>$this->input->post('clientname'),
				'clt_email'=>$this->input->post('clientemail'),
				'clt_number'=>$this->input->post('clientnumber'),
				'clt_mngr_id'=>$this->input->post('clientmanager'),
				'clt_added_by'=>$this->session->userdata('logged_in')['log_id'],	
				'ca_id'=>$this->session->userdata('logged_in')['ca_id'],
				'clt_cont_person'=>$this->input->post('clientcontactperson')?$this->input->post('clientcontactperson'):''
				);
				$result=$this->Do_client->addclient($clientdetails);
					if($result){
						if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
						echo 'Client has been added successfully';
						}
						else{
						$response=array('success'=>true,'response'=>array('message'=>'Client has been added successfully'));
						$this->response($response,OK);
						}
					}
				}	
        }
		
		#This function will load the view when the user hits the link of browser using get request.
		public function addclient_get(){
		$this->load->view('client_page');
		}
		
		#This function is used to get all the users of particular CA.
		public function getemployee_get($web_view=null){
			if($web_view){
			return $result=$this->Do_client->getemployee();
			}
			else{
			$this->response($this->Do_client->getemployee(),OK);
			}
		}
}