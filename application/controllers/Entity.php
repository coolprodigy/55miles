<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Entity extends REST_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('Do_entity');
		//$this->session->set_userdata('is_in_login_page',true);
		}

		public function index_get(){
		$this->load->view('entity_page');
		}

		public function addentity_post(){
				$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">','</div>');
				$this->form_validation->set_rules('entityname','Entity Name','required|min_length[5]');
				$this->form_validation->set_rules('clientname','Client Name','required');
				if($this->form_validation->run()==false){
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
					$this->load->view('entity_page');
					}else{
					$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
					$this->response($response,OK);
					}//Done testing
				}
			else{
				#print_r($_POST);die;
				$entity_det=array(
				'clt_grp_id'=>$this->input->post('clientname'),
				'clt_ent_name'=>$this->input->post('entityname')
				);
				$result=$this->Do_entity->addentity($entity_det);
				if($result){
					if(!isset($_POST['web_view']) && !empty($_POST['web_view'])){
						echo 'Entity has been added successfully';
					}else{
						$response=array('success'=>true,'response'=>array('message'=>'Entity has been added successfully'));
						$this->response($response,OK);
					}
				}
			}	
        }

		#This function will load the view when the user hits the link of browser using get request.
		public function addentity_get(){
		$this->load->view('entity_page');
		}
		
		#This function will list the client groups related to CA to which he wish to add entity.
		public function getclientgrp_get($web_view=null){
			if($web_view){
			return $this->Do_entity->getclientgrp();
			}	
			else{
			$this->response($this->Do_entity->getclientgrp(),OK);	
			}
		}
}