<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Login extends REST_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('Do_login');
		}

		public function index_get(){
			$this->load->view('login_page');
		}

		public function verifylogin_post(){
				$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">','</div>');
				$this->form_validation->set_rules('username','User Name/Email','required|min_length[5]|valid_email|callback_check_user');
				$this->form_validation->set_rules('password','Password','required|min_length[5]');
				if($this->form_validation->run()==false){
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
					$this->load->view('login_page');
					}else{
					$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
					$this->response($response,OK);
					}//Done testing
				}
				else{
					#echo 'success';die;
					//redirect('client','refresh');
					redirect('Profile');
					//$this->load->view('login_page');
				}//Done testing
			
		}
		
		#This function cheeks the credential for the user in the database.
		public function check_user(){
		$email=$this->input->post('username');
		$pass=md5(SALT.$this->input->post('password'));
		#print_R($_POST);die;
		$result=$this->Do_login->check_user($email,$pass);
			if($result){
				$session_user=array(
				'log_id'=>$result['log_id'],
				'status'=>$result['log_status'], #Check the subscription for payment verification.
				'role_id'=>$result['role_id'],	#Check the user type like superadmin,admin.
				'email'=>$result['log_email'],
				'ca_id'=>$result['ca_id']  #CA Id which will be assigned to each ca firm(this will attached to all the employees of particular ca firm to identify employee) who logs with us.
				);
				$this->session->set_userdata('logged_in',$session_user);
				if(isset($_POST['remember']) && !empty($_POST['remember'])){
					setcookie('login', $email, time() + (86400 * 7), "/");//Save cookie for 7 days
					setcookie('loginpwd', $pass, time() + (86400 * 7), "/");//Save cookie for 7 days
				}
				$userdetails=$this->session->userdata('logged_in');
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
						#Here we will add the view to be loaded after succesful login.
						#echo 'hi';die;
						return true;
					}
					else{
						#print_R($userdetails);
						$this->response(array('success'=>true,'users'=>array($userdetails)),OK);
					}//Done testing
				
			}
			else{
				//Done testing
				if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
						#Here we will add the view to be loaded after succesful login.
						$this->form_validation->set_message('check_user','Either Email or Password is incorrect');
						return false;
					}	
				else{
				$response=array('success'=>false,'response' =>array('message'=>'Either Email or Password is incorrect'));
				#print_r($response);die;
				$this->response($response, OK);
				}

			}
		}
}