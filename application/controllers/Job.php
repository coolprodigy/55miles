<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Job extends REST_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('Do_job');
		$this->load->model('Do_entity');
		//$this->session->set_userdata('is_in_login_page',true);
		}
		
		#this function is used to load job page
		public function index_get(){
			$this->load->view('job_page');
		}
		
		#this function is used to add the job into the database and also check for validations.
		public function addjob_post(){
				#print_R($this->input->post('useronjob'));
				$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">','</div>');
				$this->form_validation->set_rules('clientgrp','Client','required');
				//$this->form_validation->set_rules('clientent','Entity','required');
				#print_R($_POST);
				if(isset($_POST['jobtype'])) {
					$this->form_validation->set_rules('jobtype','Job Name Or Job Type','required');
				}
				//else if (isset($_POST['jobname'])){
				//	$this->form_validation->set_rules('jobname','Job Name Or Job Type12345','required');
				//}
	
				if($this->form_validation->set_rules('periodtype','Period Type','required')){
				$demo=$this->input->post('periodtype');
				//echo $demo;
				if($demo=="2" || $demo=="1"){
					$this->form_validation->set_rules('period','Period','required');
					#echo "AY & FY";
				}
				else if($demo=="3" || $demo=="4" || $demo=="5"){
					#echo "HY, Q & M";
					$this->form_validation->set_rules('year','Year','required');
				}
				}
				$this->form_validation->set_rules('jobmanager','Job Manager','required');
				#$this->form_validation->set_rules('useronjob[]','User on Job','required');
				$this->form_validation->set_rules('duedate','Due Date','required');
				/*$this->form_validation->set_rules('priority','Priority','required');
				$this->form_validation->set_rules('flag','Flag','required');*/
				if($this->form_validation->run()==false){
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
					$this->load->view('job_page');
					}else{
					$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
					$this->response($response,OK);
					}//Done testing
				}
				else{
					//print_R($_POST);die;
					if(isset($_POST['year']) && !empty($_POST['year'])){
					$prd = array($this->input->post('period').",".$this->input->post('year'));
					}else{
						$prd = array($this->input->post('period'));
					}
					$job=array(
					'prd_id'=>$this->input->post('periodtype'),
					'job_asgnd_by'=>$this->session->userdata('logged_in')['log_id'],				
					'job_mngr_id'=>$this->input->post('jobmanager')
					);
					$job_meta=array(
					'period'=>json_encode($prd),
					'user_on_job'=>json_encode($this->input->post('useronjob')),		
					'due_date'=>$this->input->post('duedate').' '.'23:59:59',	
					'priority'=>$this->input->post('priority'),	
					'flag'=>$this->input->post('flag')	
					);
					if(isset($_POST['clientent']) && !empty($_POST['clientent'])){
					$job_meta['clt_ent_id']=$this->input->post('clientent');
					}else{
					$job_meta['clt_grp_id']=$this->input->post('clientgrp');
					}
					if(isset($_POST['jobtype']) && !empty($_POST['jobtype'])){
					$job['job_type_id']=$this->input->post('jobtype');
					}else{
					$job['job_name']=$this->input->post('jobname');
					}

				$result=$this->Do_job->addjob($job,$job_meta);
					if($result){
						if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
						echo 'Job has been added successfully';
						}
						else{
						$response=array('success'=>true,'response'=>array('message'=>'Job has been added successfully'));
						$this->response($response,OK);
						}
					}
			}	
        }
		
		#this function is used to get the types of job in the database.
		public function getjobtype_get($web_view=null){
			if($web_view){
			return $this->Do_job->getjobtype();
			}else{
			$this->response($this->Do_job->getjobtype(),OK);	
			}
		}
		

		#this function is used to get the managers on job.
		public function getjobmanager_get($web_view=null){
			if($web_view){
			return $this->Do_job->getjobmanager();
			}else{
			$this->response($this->Do_job->getjobtype(),OK);	
			}
		}
		
		#this function is used to get the users on which could be given access to job
		public function getuserchoice_get($id=null,$web_view=null){
		$id=$id;
		$web_view=$web_view;
			if($id!=0){
					$result=$this->Do_job->getuserchoice($id);
						if($result){
							if($web_view){
							echo json_encode($result);
							}else{
							$this->response($result);
							}
						}
						else{
						if($web_view){
						echo 'false';
						}else{
						$response=array('success'=>false,'response'=>array('message'=>'No User Found'));
						$this->response($response,OK);
						}		
					}
		  }else{
			if($web_view){
			echo 'false';
			}else{
			$response=array('success'=>false,'response'=>array('message'=>'Please select Client Group'));
			$this->response($response,OK);
			}	
		 }		
	}
		
		#this function is used to get the default users on the job like superadmin/partner.
		public function getuserdefault_get($id=null,$web_view=null){
		$id=$id;
		$web_view=$web_view;
		if($id!=0){
		$result=$this->Do_job->getuserdefault($id);
				if($result){
					if($web_view){
					echo json_encode($result);
					}else{
					$this->response($result);
					}
				}
				else{
					if($web_view){
					echo 'false';
					}else{
					$response=array('success'=>false,'response'=>array('message'=>'No User Found'));
					$this->response($response,OK);
					}	
				}
			}
			else{
				if($web_view){
				echo 'false';
				}else{
				$response=array('success'=>false,'response'=>array('message'=>'Please select Client Group'));
				$this->response($response,OK);
				}	
			}
		}
		
		#this function is used to get the entities related to particular client group.
		public function getentity_get($id=null,$web_view=null){
			$id=$id;
			$web_view=$web_view;
			//echo $id.$web_view;die;
				if($id!=0){
				$result=$this->Do_job->getentity($id);
				//print_R($result);
						if($result){
							if($web_view){
							echo json_encode($result);
							}else{
							$this->response($result);
							}
						}
						else{
						if($web_view){
						//echo 'hi';
						echo 'false';
						}else{
						$response=array('success'=>false,'response'=>array('message'=>'No entity Found'));
						$this->response($response,OK);
						}	
					}
				}else{
					if($web_view){
					echo 'false';
					}else{
					$response=array('success'=>false,'response'=>array('message'=>'Please select Client Group'));
					$this->response($response,OK);
					}	
				}	
		}
		
		#this function fetches the period type.
		public function getprdtype_get($web_view=null)
		{
			if($web_view){
			return $this->Do_job->getprdtype();
			}else{
			$this->response($this->Do_job->getprdtype(),OK);	
			}
		}

}