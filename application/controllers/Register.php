<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Register extends REST_Controller {
		function __Construct(){
		parent::__construct();
		$this->load->model('Do_register');
		#echo $this->_detect_method();
		#$this->session->set_userdata('is_in_login_page',false);
		}
	
		#This function will load the view using get request.
		public function index_get(){
		$this->load->view('register_page');
		}
		
#This function validates the registration field and allows to insert into the database		
		public function registration_post()
        {
				$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">','</div>');
				$this->form_validation->set_rules('fullname','Full Name','required|min_length[5]|max_length[40]');
				$this->form_validation->set_rules('email','Email','required|valid_email|callback_email_check');
				$this->form_validation->set_rules('password','Password','required|min_length[5]');
				$this->form_validation->set_rules('companyname','Company Name','required|min_length[5]');
				#$this->form_validation->set_rules('g-recaptcha-response','Recaptcha','callback_recaptcha');
				if($this->form_validation->run()==false){
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
					$this->load->view('register_page');
					}else{
					$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
					$this->response($response,OK);
					}//Done testing
					
				}
			else{	
						$personal=array(
						'user_fullname'=>$this->input->post('fullname')
						);
						$credentials=array(
						'log_email'=>$this->input->post('email'),
						#'pwd'=>md5($this->input->post('pass'))
						 'pwd'=>md5(SALT.$this->input->post('password')),
						 'role_id'=>1		 
						);

						$firm=array(
						'ca_name'=>$this->input->post('companyname')	
						);
				$result=$this->Do_register->insert_customer($personal,$credentials,$firm);
				if($result){
				$message='Dear '.$personal["user_fullname"].',<br/>Your account has been successfully created.<br/>Please click on below URL or paste into your browser<br/><br/> http://55miles.in/<br/><br/>Login Information<br/>User ID:'.' '.$this->input->post('email').'<br/>Password:'.$this->input->post('password').'<br/>Thanks<br/>Admin Team';
				$this->sendmail($this->input->post('email'),$message);
					if(!isset($_POST['web_view']) && !empty($_POST['web_view'])){	
					#After successful registration he will be redirected to all jobs page.
					#echo 'hi';die;
					return true;
					}
					else{
					$this->response(array('success'=>true,'response'=>array('message'=>'User has been registered')),OK);
					}
				}
			}
			
        }


		#This function will check for the email in the database.
		public function email_check(){
		$email=$this->input->post('email');
		$result=$this->Do_register->email_check($email);
		if($result){
			if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
			$this->form_validation->set_message('email_check', 'Email is already Registered');
			return FALSE;
			}
			else{
			$response=array('success'=>false,'response' =>array('message'=>'Email is already Registered!'));
			#print_r($response);die;
			$this->response($response, 404);	
			}
		}
		else{
		return true;
		}
	}//Done testing
		
	public function sendmail($to,$message){
		$this->load->library('email');
		$config['protocol']='smtp';
		$config['smtp_host']='ssl://smtp.googlemail.com';
		$config['smtp_port']='465';
		$config['smtp_timeout']='30';
		$config['smtp_user']='infotestng@gmail.com';
		$config['smtp_pass']='9757252991';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from('infotestng@gmail', '55miles');
		$this->email->to($to);
		$this->email->subject('Retrieve Your Password');
		$this->email->message($message);
		#$this->email->send();
		if($this->email->send())
         {
          return true;
         }
         else
        {
         show_error($this->email->print_debugger());
        }
	}	

		#This function will load the view when the user hits the link of browser using get request.
		public function registration_get(){
		$this->load->view('register_page');
		}

		#Googles recaptcha library.
		/*public function recaptcha($str='')
		{
		$this->load->library('Curl');
		$google_url="https://www.google.com/recaptcha/api/siteverify";
		$secret='6LflewoTAAAAAMJzAU1UMaaaFTgDhqYtReVsIRav';
		$ip=$_SERVER['REMOTE_ADDR'];
		//$ip='127.0.01';
		#echo $ip;
		$url=$google_url."?secret=".$secret."&response=".$str."&remoteip=".$ip;
		#echo $url;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); #you're attempting to connect via SSL, you need to adjust your cURL options to handle it.Setting CURLOPT_SSL_VERIFYPEER to false will make it so that it accepts any certificate given to it rather than verifying them.
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/23.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
		$res = curl_exec($curl);
		curl_error($curl);
		curl_close($curl);
		$res= json_decode($res, true);
		#var_dump($res);
		//reCaptcha success check
		if($res['success'])
		{
		  return TRUE;
		}
		else
		{	
		  if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
		  $this->form_validation->set_message('recaptcha', 'The reCAPTCHA field is telling me that you are a robot. Shall we give it another try?');
		  return FALSE;
		  }else{
			$response=array('success'=>false,'error' =>array('message'=>'The reCAPTCHA field is telling me that you are a robot. Shall we give it another try?'));
			#print_r($response);die;
			$this->response($response, 404);	
		  }
		}
	}*/
}