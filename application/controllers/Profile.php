<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Profile extends REST_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('Do_profile');
		}

		public function index_get(){
		$controller['object']=$this;	
		$this->load->view('profile_page',$controller);
		}
		
		public function userdetails_get($web_view=null){
			if($web_view){
			return $this->Do_profile->userdetails();
			}
			else{
			$this->response($this->Do_profile->userdetails(),OK);
			}
		}
		
		public function edituserdetails_post(){
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">','</div>');
		$this->form_validation->set_rules('fullname','Full Name','required|min_length[5]|max_length[40]');
		$this->form_validation->set_rules('email','Email','required|valid_email');
			if($this->form_validation->run()==false){
				if(!isset($_POST['web_view']) && !empty($_POST['web_view'])){
					$this->load->view('profile_page');
				}else{
					$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
					$this->response($response,OK);
				}
			}else{
				$userid=$this->input->post('userid');
				$fullname=array(
						'user_fullname'=>$this->input->post('fullname')
						);
				$email=array(
					'log_email'=>$this->input->post('email')	
					);
				$result=$this->Do_profile->edituserdetails($fullname,$email,$userid);
				if($result){
					if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
					redirect('Profile');
					}else{
					$this->response(array('success'=>true,'response'=>array('message'=>'Information Updated')),OK);
					#The user must be redirected to login page.
					}
				}
		
			}
		}
}