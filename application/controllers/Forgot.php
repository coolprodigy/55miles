<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Forgot extends REST_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Do_forgot');
		$this->load->library('email');
	}

	#This function is used to display the forgot page.
	public function index_get()
	{
		$this->load->view('forgot_page');
	}
	
	#This function is used for validation purpose. 	
	public function getusername_post(){
	$this->form_validation->set_error_delimiters('<div class="error" style="color: red;">', '</div>');
		$this->form_validation->set_rules('username','Username/Email','required|valid_email|callback_chkemail');
			if($this->form_validation->run()==False){
				if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
				$this->load->view('forgot_page');
				}
				else{
					$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
					$this->response($response,404);
				}//Done testing
			}else{
				$msg='Instruction for signing have been emailed to you';
				if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
				$this->session->set_userdata('web_view',true);
				$this->session->set_flashdata('msg',$msg);
				redirect('Login');
				}
				else{
				$response=array('success'=>true,'response' =>array('message'=>'Instruction for signing have been emailed to you'));
				$this->response($response, OK);
				}
			}
	}

	public function chkemail(){
	$email=$this->input->post('username');
	$result=$this->Do_forgot->chkemail($email);
		if($result){
			$message="Hi ".$result['user_fullname'].",<br/>Can't remember your password? Dont worry about it -- it happens.<br/><br/> Login Information<br/>Your Username is:"." ".$email."<br/><a href='".SITE_URL."Forgot/resetpwd/".$result['log_id']."'>Click on the link below to set your password</a>.<br/>Thanks<br/>Admin Team";
			$this->sendmail($email,$message);
				if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
						return true;
					}
					else{
						$this->response(array('success'=>true,'response'=>array('message'=>'User Exist')),OK);
					}
		}else{
			if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
				$this->form_validation->set_message('chkemail', 'No email with these record');
				return false;
			}
			else{
			$response=array('success'=>false,'response'=>array('message'=>'No email with these record'));
			$this->response($response,OK);
			}
		}
	}
	
	#this function is used to sendmail to reset his password.
	public function sendmail($to,$message){
		$this->load->library('email');
		$config['protocol']='smtp';
		$config['smtp_host']='ssl://smtp.googlemail.com';
		$config['smtp_port']='465';
		$config['smtp_timeout']='30';
		$config['smtp_user']='infotestng@gmail.com';
		$config['smtp_pass']='9757252991';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from('infotestng@gmail', '55miles');
		$this->email->to($to);
		$this->email->subject('Retrieve Your Password');
		$this->email->message($message);
		#$this->email->send();
		if($this->email->send())
         {
          return true;
         }
         else
        {
         show_error($this->email->print_debugger());
        }
	}
	
	#This function will get the reset password page.
	public function resetpwd_get($id=null){
		#echo 'Trying to set the password';
		if($this->session->userdata('web_view')){
		$this->session->set_userdata('userid',$id);
		$this->load->view('password_page');
		}else{
		$this->response(array('success'=>true,'userid'=>$id),OK);
		}
	}
	
	public function setnewpwd_post(){
	$this->form_validation->set_error_delimiters('<div class="error" style="color: red;">', '</div>');
	$this->form_validation->set_rules('pwd','Password','required|min_length[5]|matches[cpwd]');
	$this->form_validation->set_rules('cpwd','Confirm Password','required|min_length[5]');
		if($this->form_validation->run()==false){
			if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
			$this->load->view('password_page');
			}else{
			$response=array('success'=>false,'response'=>array($this->form_validation->error_array()));
			$this->response($response,OK);
			}
		}
		else{
			$data=array(
			'pwd'=>md5(SALT.$this->input->post('pwd'))	
			);
			$log_id=$this->input->post('userid');
			$result=$this->Do_forgot->setnewpwd($data,$log_id);
			#echo 'hello';die;
			if($result){
				if(isset($_POST['web_view']) && !empty($_POST['web_view'])){
					$this->session->set_flashdata('pwd_set','Your Password has been set.Please Login...');
					redirect('Login');
				}
				else{
					$this->response(array('success'=>true,'response'=>array('message'=>'Your Password has been set.Please Login...')),OK);
				}
			}		
		}
	}
}